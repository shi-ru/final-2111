花倩   微信：18612483268

gitee网址：https://gitee.com/huadahua/final-2111.git

五天课程安排：

  2-3天：刷题（复杂sql，一定手写）

 3-4：知识点

1. 冒泡排序 -- 手写代码
2. 事务的隔离级别 -- 面试热点
3. 索引 -- 热点
4. JVM内存结构以及如何调优 -- 热点
5. GC垃圾回收机制的常用算法  --热点
6. redis五种数据类型以及应用 -- 热点

4-5天：19道sql题目（难度：难）



### 数据库

1. 数据库有两张表为user表和user_role表，执行：SELECT u.*,r.`name` FROM USER u LEFT JOIN user_role r ON u.`id` = r.user_id 之后获取到的结果为:(A)

   ```
    	user表数据:             user_role表数据:
       id username             id name user_id
       1  张三                  1 售后   1
       2  李四                  2 运维   2
       3  王五                  3 研发   3
       4  小刘                  4 实施   5 
   A.  1 张三 售后  
       2 李四 运维 
       3 王五 研发 
       4 小刘 null
   B.  1 张三 售后 
       2 李四 运维 
       3 王五 研发
   C.  1 张三 售后 
       2 李四 运维 
       3 王五 研发
       null null 实施
       
   连接查询：
   	内连接： （inner） join   当查询满足所有条件的数据时
   	外连接：
   		left  join：左表为主表（主表中的所有数据均显示）
   		right  join：右表为主表（主表中的所有数据均显示）
   ```

#### sql编写题：

1. 从表t1(有字段id,name,sex,score)中取出sex为男，score列前十名的name字段

   ~~~mysql
   select name from t1 where sex='男' order by score desc limit 10;
   limit用法：
   1. limit m,n  分页   m:从哪条数据开始查询（从0开始）  n:查询多少条数据
      limit  m   筛选前m条数据
   ~~~

2. 请描述查询语句中各部分的执行顺序

   ```mysql
   select  
   ..from A join B on a.xx=b.xx and b.xx=c.xx where xx=? and xx=? group by xx having xx=?  order by ... limit m,n
   
   where:  查询条件   是在分组之前就进行筛选
   group by   :分组   适用于：统计每个班级的总人数/平均分  通常会和聚合函数配合使用
   having： 筛选条件/查询条件   对分组后的结果再次进行筛选
   
   
   1. from join   产生虚拟表v1  - 产生的是笛卡尔积
   	笛卡尔积：两表完全连接的结果集（不具有任何逻辑意义）
   2. on    根据连接条件进行筛选,基于V1,产生虚拟表v2   
   3. left/right join 若是外连接,将主表中的所有数据进行补充到v2表中
   4. where   根据查询条件,从v2中进行数据筛选,产生虚拟表v3
   5. group by  对v3分组,产生虚拟表v4
   6. having    对分组后的结果再次筛选 ,产生虚拟表v5
   7. select    选择最终要显示的字段,产生虚拟表v6
   8. distinct  对结果集进行去重,产生虚拟表v7
   9. order by  对结果根据某字段进行升序/降序排列 
   10. limit    选择需要的数据
   ```

3. 表名：stu_score

| id   | name | score | bj   |
| ---- | ---- | ----- | ---- |
| 1    | 赵一 | 89    | 1班  |
| 2    | 钱二 | 88    | 2班  |
| 3    | 孙三 | 84    | 1班  |
| 4    | 李四 | 86    | 2班  |
| 5    | 王五 | 87    | 1班  |
| 6    | 吴六 | 91    | 2班  |

获取每个班级的最后一名  -- 表在final.sql文件的最后

~~~mysql
使用嵌套查询
1. 先查询出每个班级的最低分
2. 根据以上的查询结果，和原表进行联查，查询每个班级最低分对应的学生是谁
SELECT ss.* FROM stu_score ss JOIN 
(SELECT bj,MIN(score) min FROM stu_score GROUP BY bj)tmp
ON ss.bj=tmp.bj AND ss.score=tmp.min
~~~

4. 表user，字段有：id（非自增，不连续），age，name，sex，addTime。求创建时间（addTime）在2020-05-22至2020-06-17之间所有数据

   ~~~mysql
   select * from user where addTime between '2020-05-22' and '2020-06-17';
   between ...and...   两个边界值都包括
   
   查询某段日期之间的数据，可以使用between。。。and，也可以使用>=  <=
   
   Java中有一系列方法，会涉及道两个边界值，但是Java中出现两个边界值，都是包头不包尾
   ~~~

5. 基于表user，查出表中第10条至第20条连续分页数据的sql语句

   ~~~mysql
   select * from user limit 9,11
   ~~~

   

##### 5. 有以下表以及数据，写出以下sql

student表:

| sno  | sname  | sage |
| ---- | ------ | ---- |
| 1    | 周杰伦 | 18   |
| 2    | 周润发 | 18   |
| 3    | 吴孟达 | 25   |
| 4    | 刘德华 | 25   |
| 5    | 李连杰 | 29   |

| cno  | cname |
| ---- | ----- |
| 1    | 语文  |
| 2    | 数学  |
| 3    | 英语  |

| sno  | cno  | score |
| ---- | ---- | ----- |
| 1    | 1    | 60    |
| 1    | 2    | 61    |
| 2    | 1    | 80    |

三张表分别为:student ,course,score

1. 查询所有学生的学号，姓名，选课数，总成绩

   ~~~mysql
   思路：查询所有学生的信息，所以以学生表为主表和成绩表进行外连接查询，根据学生表的学号分组，统计每个学生的选课数和总成绩（注意：一定要根据学生表的学号分组，不能根据成绩表的学号分组）
   
   注意点：count(col)  统计col字段的值有多少个，若该字段的值为null，则统计结果为0
   
   select s.sno,sname,count(cno),sum(score) from student s left join score sc on s.sno=sc.sno group by s.sno（这种方式若总成绩为null，则显示null）
   或
   select s.sno,sname,count(cno),IFNULL(sum(score),0) su from student s left join score sc on s.sno=sc.sno group by s.sno（这种方式总成绩为null，则替换为0）
   
   注意点：
   - ifNull(col,0)  某字段的值为null，则替换为0
   - 写查询语句，能不用嵌套就不用嵌套，尽量用联查实现。（嵌套查询会产生中间表，执行效率低于联查）
   - count(*/col)  结果不可能为null，结果是整数（0/正整数）
   ~~~

2. 查询没有学完所有课程的学生学号，姓名

   ~~~mysql
   筛选选修课程数<总课程数的学生
   1. 查询总课程数
        select count(*) from course1
   2. 先统计每个学生的选课数（选课数为0的学生也要统计），对以上分组后的结果再次进行筛选，筛选课程数<总课程数的学生
   SELECT s.sno,sname FROM student s LEFT JOIN score sc ON s.sno=sc.sno
   GROUP BY s.sno HAVING COUNT(cno)<(SELECT COUNT(*) FROM course1)
   ~~~

##### 6. 已知以下表结构

班级表（class）

| 编号 | 班级名称   |
| ---- | ---------- |
| id   | class_name |
| 1    | 一班       |
| 2    | 二班       |
| 3    | 三班       |
| 4    | 四班       |
| 5    | 五班       |

学生表（student）

| 编号 | 学号    | 姓名     | 性别       | 所属班级 |
| ---- | ------- | -------- | ---------- | -------- |
| id   | stu_no  | stu_name | stu_gender | class_id |
| 1    | 2020001 | 张三     | 男         | 1        |
| 2    | 2020002 | 李四     | 男         | 1        |
| 3    | 2020003 | 李丽     | 女         | 2        |
| 4    | 2020004 | 赵婷     | 女         | 3        |
| 5    | 2020005 | 王五     | 男         | 3        |

成绩表（score）

| 学生   | 语文    | 数学 |
| ------ | ------- | ---- |
| stu_id | chinese | math |
| 1      | 70      | 47   |
| 2      | 80      | 60   |
| 3      | 50      | 82   |
| 4      | 80      | 90   |

业务场景限制：

1. 一个班级有多名学生，一名学生只属于一个班级
2. 学生有可能没有成绩

###### 题目：

1. 查询所有学生的信息（学号，姓名，性别，班级名称）

   ~~~mysql
   select stu_no,stu_name,stu_gender,class_name from student s join class c on s.class_id=c.id
   ~~~

2. 查询所有人(包括没有成绩的学生)的课程分数（学号，姓名，性别，班级名称，语文分数，数学分数）

   ~~~mysql
   多表连接查询的语法：
   	1. 多表内连接：
   		 select....from  a join b join c on a.xx=b.xx and b.xx=c.xx where...--效率更高
   		 select...from a join b on a.xx=b.xx join c on b.xx=c.xx where....--效率稍低
   		 
   		 效果一样，但是效率不一样。
       2. 多表联查，有内连，有外连，语法为：
       	select...from a join b on a.xx=b.xx left join c on b.xx=c.xx
       	
      select stu_no,stu_name,stu_gender,class_name,chinese,math from student s join class c on s.class_id=c.id left join score sc on s.id=sc.stu_id
   ~~~

3. 查询语文分数比“张三”高的学生（学号，姓名，性别，班级名称，语文分 数）

   ~~~mysql
   1. 查询张三的语文分数
      select chinese from student s join score sc on s.id=sc.stu_id where stu_name='张三'
   2. 根据以上的结果查询语文分数大于张三语文分数的学生（三表内连接查询）
      select stu_no,stu_name,stu_gender,class_name,chinese from score sc join student s join class c on sc.stu_id=s.id and s.class_id=c.id where sc.chinese>(select chinese from student s join score sc on s.id=sc.stu_id where stu_name='张三')
   ~~~

4. 查询各科都合格（分数>=60）的学生（学号，姓名，语文分数，数学分数）

   ~~~mysql
   select stu_no,stu_name,chinese,math from student s join score sc on sc.stu_id=s.id where chinese>=60 and math>=60
   ~~~

5. 查询班级人数>=2的班级（班级编号，班级名称，人数）

   ~~~mysql
   select class_id,class_name,count(stu_no) cou from student s join class c on s.class_id=c.id group by class_id having cou>=2
   ~~~

#### 4. 有一张表score，三个字段名，姓名，课程，分数，数据如下，请写一条sql语句，查询出每门课程都大于等于80分的学生信息

| name | course | score |
| ---- | ------ | ----- |
| 张三 | 语文   | 81    |
| 张三 | 数学   | 75    |
| 李四 | 语文   | 76    |
| 王五 | 语文   | 81    |
| 王五 | 数学   | 100   |
| 王五 | 英语   | 90    |

~~~mysql
表数据在final031.sql中
思路：查询每个学生的最低分，筛选最低分大于等于80的学生
select name from score1 group by name having min(score)>=80
~~~

#####  5. 有一张表student，包括字段id和name，请写一条sql语句，将表中name字段中重复的记录删除，只保留重复数据中的id最大的那一条数据。

| id   | name |
| ---- | ---- |
| 1    | 张三 |
| 2    | 张三 |
| 3    | 李四 |
| 4    | 王五 |
| 5    | 王五 |
| 6    | 王五 |

要求只留下：2 张三， 3 李四， 6 王五 这三条记录

~~~mysql
1. 查询出每个人名的最大id
	select max(id) from student1 group by name
2. 从表中删除数据，条件是删除非以上数据
    delete from student1 where id not in(select max(id) from student1 group by name)
    
 mysql版本不是mariadb10.xx以上，则以上sql执行失败
~~~

##### 6.  学生表如下

| 自动编号 | 学号    | 姓名 | 课程编号 | 课程名称 | 分数 |
| -------- | ------- | ---- | -------- | -------- | ---- |
| 1        | 2021001 | 张三 | 0001     | 数学     | 69   |
| 2        | 2021002 | 李四 | 0001     | 数学     | 89   |
| 3        | 2021001 | 张三 | 0001     | 数学     | 69   |

- 删除除了自动编号不同,其它都相同的学生冗余信息

  ~~~mysql
  delete from student2 where id not in(select min(id) from student2 group by stu_no)
  ~~~

  

##### 7. 有如下一张表,按照右侧的结果格式写出生成右侧结果的SQL语句

![img](https://gitee.com/huadahua/final-2010/raw/master/docs/day09/%E6%95%B0%E6%8D%AE%E5%BA%93/1.png)

~~~mysql
表在match_record.sql中
select match_date '比赛日期',
(select count(*) from match_record m1 where m1.match_date=m.match_date and result='胜')'胜',
(select count(*) from match_record m2 where m2.match_date=m.match_date and result='负')'负'
from match_record m group by match_date

或者
  使用：case when ...then 1 else 0 end
用法： eg： sum(case when score>=60 then 1 else 0 end)
	count(case when score>=60 and score<=80 then 1 end )
	
SELECT match_date '比赛日期',
COUNT(case when result='胜' then 1 END) '胜',
COUNT(case when result='负' then 1 END) '负'
FROM match_record GROUP BY match_date
~~~

##### 8. 有如下3张表,完成如下需求:

![img](https://gitee.com/huadahua/final-2010/raw/master/docs/day09/%E6%95%B0%E6%8D%AE%E5%BA%93/2.png)

(1) 用户表中添加一条数据,请写出SQL语句

~~~mysql
insert into user values('s1001','tom','男',23,'1992-12-12',1);
~~~

(2) 在用户表中把姓名为"李四"的在职用户的年龄修改为28

~~~mysql
语法： update  tableA set xx=?,xx=?,xx=? where xx=? and xx=?
注意点：修改的若为多个字段，之间不能用and连接，需要用，连接

update user set age=28 where name='李四' and state=1
~~~

(3) 查询所有的角色名称为"技术人员",性别是女的在职人员

~~~mysql
典型的三表内连接查询，加查询条件

select u.* from role r join user_role ur join user u on r.id=ur.role_id and ur.user_id=u.id where r.name='技术人员' and sex='女' and state=1
~~~

(4) 把角色名称为"研发人员"的用户"张三"从用户表中删除

~~~mysql
delete from user where id=?  删除一条数据
delete from user where id in (...)

1. 查询角色名称是研发人员的张三的用户id
2. 根据以上查询的用户id删除数据即可

delete from user where id in(select u.id from role r join user_role ur join user u on r.id=ur.role_id and ur.user_id=u.id where r.name='研发人员' and u.name='张三')
~~~

(5) 查询年龄在18到24岁之间的男性在职人员

(6)查询年龄为18,19,20的男性和女性员工的人数,结果如下图所示:

| 年龄 | 男   | 女   |
| ---- | ---- | ---- |
| 18   | 5    | 3    |
| 19   | 4    | 6    |
| 20   | 1    | 0    |

(7) 查询前50名姓张的男员工

(8）查询所有根角色员工,根角色就是父角色为null的角色.

##### 9. 以下四个表，表名称及表结构如下：

```
student(sno,sname,sage,ssex) 学生表
course(cno,cname,tno) 课程表
sc(sno,cno,score) 成绩表
teacher(tno,tname) 教师表
```

1. 查询课程1的成绩比课程2的成绩高的所有学生的信息
2. 查询平均成绩大于60分的同学的学号和平均成绩
3. 查询学过‘李四’老师所教课程的所有同学的学号，姓名
4. 查询姓“李”得老师的个数
5. 查询每门课程的选修人数(课程名称，学生数量)--存在没有人选的课程
6. 删除“1002”同学的“1”课程的成绩
7. 查询选修人数最多的课程（课程id，课程名称，学生数量）--考虑有多门课程都是选修最多的情况

##### 10. 数据库表名为guest，请简答

| 账号     | 消费    | 时间       | 金额  | 班次  |
| -------- | ------- | ---------- | ----- | ----- |
| accounts | details | date       | money | class |
| s0001    | 房费    | 2020-01-01 | 280   | 001   |
| s0001    | 酒水    | 2020-01-02 | 120   | 001   |
| s0001    | 房费    | 2020-01-08 | 300   | 003   |
| s0002    | 酒水    | 2020-01-29 | 50    |       |
| s0003    | 房费    | 2020-01-31 | 180   | 002   |
| s0004    | 房费    | 2020-02-01 | 230   | 001   |
| s0005    | 酒水    | 2020-02-01 | 100   |       |
| s0005    | 房费    | 2020-02-02 | 128   | 001   |

1. 查询出房费都大于200的账号
2. 查询出1月份每个账号酒水和房费的总金额
3. 将不是房费的班次都更改为‘001’
4. 查询出消费都大于100的账号

##### 3. 完成以下sql查询

下面是学生成绩表(student_score)结构说明

| 字段名称       | 字段解释           | 字段类型              | 字段长度 |
| -------------- | ------------------ | --------------------- | -------- |
| student_id     | 学号               | 字符                  | 8        |
| student_name   | 姓名               | 字符                  | 50       |
| student_gender | 性别               | 字符(男/女)           | 4        |
| course_id      | 课程号             | 字符                  | 5        |
| score          | 分数               | 数值                  | 3        |
| ismakeup       | 当前考试是否为补考 | 字符(补考:1;非补考:0) | 2        |

下面是课程表(course)说明

| 字段名称    | 字段解释 | 字段类型 | 字段长度 | 约束     |
| ----------- | -------- | -------- | -------- | -------- |
| course_id   | 课程号   | 字符     | 5        | PK       |
| course_name | 课程名   | 字符     | 30       | Not null |
| course_desc | 课程介绍 | 字符     | 60       |          |

1、查找第一次考试后所有需要补考(小于60分)的学生姓名和这门课程的名称和成绩；

2、查询每个学生第一次考试后需要补考(小于60分)的课程平均分和科目数

3、查询所有参加了补考的学生的学生姓名，课程名称，补考成绩和非补考成绩；

##### 6. 有如下表：

emp:

empno int(员工编号) ,ename varchar(50)(员工姓名) ，job varchar(100) （工作岗位），mgr int (上级领导编号)，hiredate date（雇佣日期），sal int（薪金），comm int（佣金） deptno int (部门编号)

提示：工资=薪金+佣金

dept表：

deptno int (部门编号) ， dname 部门名称 loc 地点

1. 列出在每个部门工作的员工数量，平均工资
2. 列出所有员工的姓名，部门名称和工资
3. 列出所有部门的详细信息和部门人数
4. 列出各种工作的最低工资
5. 列出各个部门的manager的最低薪金(若是manager,其job的值为manageer)
6. 列出受雇日期早于其直接上级的所有员工
7. 列出部门名称和这些部门的员工信息，同时列出那些没有员工的部门
8. 列出所有‘clerk’（办事员）岗位的姓名以及部门名称
9. 列出最低薪金大于6500的各种工作
10. 列出在研发部工作的员工的姓名，假定不知道研发部的部门编号
