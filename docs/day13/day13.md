##### 8. 有如下3张表,完成如下需求:

![img](https://gitee.com/huadahua/final-2010/raw/master/docs/day09/%E6%95%B0%E6%8D%AE%E5%BA%93/2.png)

(1) 用户表中添加一条数据,请写出SQL语句

~~~mysql
insert into user values('s1001','tom','男',23,'1992-12-12',1);
~~~

(2) 在用户表中把姓名为"李四"的在职用户的年龄修改为28

~~~mysql
语法： update  tableA set xx=?,xx=?,xx=? where xx=? and xx=?
注意点：修改的若为多个字段，之间不能用and连接，需要用，连接

update user set age=28 where name='李四' and state=1
~~~

(3) 查询所有的角色名称为"技术人员",性别是女的在职人员

~~~mysql
典型的三表内连接查询，加查询条件

select u.* from role r join user_role ur join user u on r.id=ur.role_id and ur.user_id=u.id where r.name='技术人员' and sex='女' and state=1
~~~

(4) 把角色名称为"研发人员"的用户"张三"从用户表中删除

~~~mysql
delete from user where id=?  删除一条数据
delete from user where id in (...)

1. 查询角色名称是研发人员的张三的用户id
2. 根据以上查询的用户id删除数据即可

delete from user where id in(select u.id from role r join user_role ur join user u on r.id=ur.role_id and ur.user_id=u.id where r.name='研发人员' and u.name='张三')
~~~

(5) 查询年龄在18到24岁之间的男性在职人员

~~~mysql
select * from user where age between 18 and 24 and sex='男' and state=1;
~~~

(6)查询年龄为18,19,20的男性和女性员工的人数,结果如下图所示:

| 年龄 | 男   | 女   |
| ---- | ---- | ---- |
| 18   | 5    | 3    |
| 19   | 4    | 6    |
| 20   | 1    | 0    |

~~~mysql
与比赛表中统计每天胜负的场次是同一体型，解法是相同的。
思路：
1. 先筛选处于18，19，20年龄的学生
2. 对以上数据进行分组，统计各年龄男女的人数

select age '年龄',
(select count(*) from student s1 where s1.age=s.age and stu_gender='男')'男',
(select count(*) from student s2 where s2.age=s.age and stu_gender='女')'女'
from student s where age in(18,19,20) group by age

或者
select age '年龄',
count(case when stu_gender='男' then 1 end) '男',
count(case when stu_gender='女' then 1 end) '女'
from student where age in(18,19,20) group by age
~~~

(7) 查询前50名姓张的男员工

~~~mysql
select * from user where name like '张%' and sex='男' limit 50
~~~

(8）查询所有根角色员工,根角色就是父角色为null的角色.

~~~mysql
考点：xx=null  错误的    xx  is null  
select u.* from role r join user_role ur join user u on r.id=ur.role_id and ur.user_id=u.id where parentId is null
~~~

##### 9. 以下四个表，表名称及表结构如下：--表在final031.sql中

```
student(sno,sname,sage,ssex) 学生表
course(cno,cname,tno) 课程表
sc(sno,cno,score) 成绩表
teacher(tno,tname) 教师表
```

1. 查询课程1的成绩比课程2的成绩高的所有学生的信息

   ~~~mysql
   考点：表的自连接查询
   思路：将sc表当成2份，一份视为课程1的成绩表，一份视为课程2的成绩表，两表连接，连接条件为学号相等，且cno=1表中的成绩大于cno=2表中的成绩，得到的学号即为查询的学号，最后和学生表联查，获取对应的学生信息即可。
   SELECT s.*,sc1.score '课程1',sc2.score '课程2' FROM sc sc1 JOIN sc sc2 JOIN student s ON sc1.sno=sc2.sno AND 
   sc1.score>sc2.score AND sc1.sno=s.sno WHERE sc1.cno=1 AND sc2.cno=2
   ~~~

2. 查询平均成绩大于60分的同学的学号和平均成绩

   ~~~mysql
   select sno,avg(score) av from sc group by sno having av>60
   ~~~

3. 查询学过‘李四’老师所教课程的所有同学的学号，姓名

   ~~~mysql
   需要注意对结果集进行去重
   SELECT distinct s.sno,sname  from teacher t join course c join sc join student s on t.tno=c.tno and c.cno=sc.cno and sc.sno=s.sno where tname='李四'
   ~~~

4. 查询姓“李”得老师的个数

   ~~~mysql
   select count(*) from teacher where tname like '李%'
   ~~~

5. 查询每门课程的选修人数(课程名称，学生数量)--存在没有人选的课程

   ~~~mysql
   分析得出：本题需要使用外连接查询，课程表为主表
   select c.cno,cname,count(sno) from course c left join sc on c.cno=sc.cno group by c.cno
   ~~~

6. 删除“1002”同学的“1”课程的成绩

   ~~~mysql
   delete from sc where sno='1002' and cno=1
   ~~~

7. 查询选修人数最多的课程（课程id，课程名称，学生数量）--考虑有多门课程都是选修最多的情况

   ~~~mysql
   1.统计每门课程的选修人数，获取选修人数最多是多少
   	SELECT COUNT(sno) cou FROM sc GROUP BY cno ORDER BY cou DESC LIMIT 1
   2.获取选修人数=以上查询结果人数的课程
   	统计每门课程的选修人数，筛选人数=以上查询结果的课程
     select c.cno,cname,count(sno) cou from sc join course c on sc.cno=c.cno group by sc.cno having cou=(SELECT COUNT(sno) cou FROM sc GROUP BY cno ORDER BY cou DESC LIMIT 1)
   ~~~

##### 10. 数据库表名为guest，请简答

| 账号     | 消费    | 时间       | 金额  | 班次  |
| -------- | ------- | ---------- | ----- | ----- |
| accounts | details | date       | money | class |
| s0001    | 房费    | 2020-01-01 | 280   | 001   |
| s0001    | 酒水    | 2020-01-02 | 120   | 001   |
| s0001    | 房费    | 2020-01-08 | 300   | 003   |
| s0002    | 酒水    | 2020-01-29 | 50    |       |
| s0003    | 房费    | 2020-01-31 | 180   | 002   |
| s0004    | 房费    | 2020-02-01 | 230   | 001   |
| s0005    | 酒水    | 2020-02-01 | 100   |       |
| s0005    | 房费    | 2020-02-02 | 128   | 001   |

1. 查询出房费都大于200的账号

   ~~~mysql
   思路：
   	1. 筛选所有的房费消费记录
   	2. 对筛选后的数据进行分组，统计每个账号房费的最低消费金额，筛选最低消费金额大于200的账号	
   select accounts from guest where details='房费' group by accounts having min(money)>200
   
   同学的解法：
   	select distinct accounts from guest where money>200 and details='房费'  --错误
   	该sql的结果：查询有房费消费大于200的账号
   ~~~

2. 查询出1月份每个账号酒水和房费的总金额

   ~~~mysql
   获取1月份数据：2种做法：
   解法1：between '2020-01-01' and '2020-01-31'
   解法2：使用日期函数
   	获取日期中的年份值   year(col)   要求：col数据类型一定是date/datetime
       获取日期中的月份值	month(col)
       获取日期中的日期值   day(col)
       获取日期时间中的小时数：  hour(col)   要求： col数据类型一定是datetime/time
       获取分钟数：         minute(col)
       获取秒数：           second（col）
       获取日期            date（col）   eg:col类型为datetime，使用date函数即可获取date数据
       
   解法1：--就题论题 -- 不建议
     select accounts,sum(money) from guest where month(date)=3 group by accounts
     
   解法2：--更准确的--建议使用
   	select accounts,
   	sum(case when details in('房费','酒水') then money else 0 end) from guest where month(date)=3 group by accounts
   ~~~

3. 将不是房费的班次都更改为‘001’

   ~~~mysql
   数据库中的不等于： ！=   也可以为<>
   update guest set class='001' where details!='房费'
   ~~~

4. 查询出消费都大于100的账号

   ~~~mysql
   select accounts from guest group by accounts having min(money)>100
   ~~~

##### 3. 完成以下sql查询

下面是学生成绩表(student_score)结构说明

| 字段名称       | 字段解释           | 字段类型              | 字段长度 |
| -------------- | ------------------ | --------------------- | -------- |
| student_id     | 学号               | 字符                  | 8        |
| student_name   | 姓名               | 字符                  | 50       |
| student_gender | 性别               | 字符(男/女)           | 4        |
| course_id      | 课程号             | 字符                  | 5        |
| score          | 分数               | 数值                  | 3        |
| ismakeup       | 当前考试是否为补考 | 字符(补考:1;非补考:0) | 2        |

下面是课程表(course)说明

| 字段名称    | 字段解释 | 字段类型 | 字段长度 | 约束     |
| ----------- | -------- | -------- | -------- | -------- |
| course_id   | 课程号   | 字符     | 5        | PK       |
| course_name | 课程名   | 字符     | 30       | Not null |
| course_desc | 课程介绍 | 字符     | 60       |          |

1、查找第一次考试后所有需要补考(小于60分)的学生姓名和这门课程的名称和成绩；

~~~mysql
select student_name,course_name,score from student_score ss join course c on ss.course_id=c.course_id  where is_makeup=0 and score<60
~~~

2、查询每个学生第一次考试后需要补考(小于60分)的课程平均分和科目数

~~~mysql
1. 先进行条件筛选
2. 对以上数据进行统计，统计每个学生第一次考试后需要补考的课程平均分和科目数
select student_id,student_name,avg(score),count(course_id) from student_score where is_makeup=0 and score<60 group by student_id
~~~

3、查询所有参加了补考的学生的学生姓名，课程名称，补考成绩和非补考成绩；

~~~mysql
考点：表的自联结查询
select sc1.student_name,course_name,sc1.score '补考成绩',sc0.score '非补考成绩' from student_score sc1 join student_score sc0 join course c on sc1.student_id=sc0.student_id and sc1.course_id=sc0.course_id and sc1.course_id=c.course_id where sc1.is_makeup=1 and sc0.is_makeup=0
~~~

##### 6. 有如下表：

emp:

empno int(员工编号) ,ename varchar(50)(员工姓名) ，job varchar(100) （工作岗位），mgr int (上级领导编号)，hiredate date（雇佣日期），sal int（薪金），comm int（佣金） deptno int (部门编号)

提示：工资=薪金+佣金

dept表：

deptno int (部门编号) ， dname 部门名称 loc 地点

1. 列出在每个部门工作的员工数量，平均工资

   ~~~mysql
   使用外连接查询
   select d.id,d.name,count(e.id)'员工数量',avg(salary+comm)'平均工资' from dept d left join employee e on d.id=e.dept_id group by d.id
   ~~~

2. 列出所有员工的姓名，部门名称和工资

   ~~~mysql
   select e.name,d.name,salary+comm '工资' from employee e join dept d on e.dept_id=d.id 
   ~~~

3. 列出所有部门的详细信息和部门人数

   ~~~mysql
   使用外连接查询，部门表为主表
   select d.*,count(e.id) from dept d left join employee e on d.id=e.dept_id group by d.id
   ~~~

4. 列出各种工作的最低工资

   ~~~mysql
   典型分组的应用
   select job,min(salary+comm)'最低工资' from employee group by job
   ~~~

5. 列出各个部门的manager的最低薪金(若是manager,其job的值为manageer)

   ~~~mysql
   select dept_id,min(salary) from employee where job='manager' group by dept_id
   ~~~

6. 列出受雇日期早于其直接上级的所有员工

   ~~~mysql
   使用表的自连接查询，筛选员工入职日期早于其直接上级入职日期的数据
   select e.id,e.name,e.hiredate,mgr.id,mgr.name,mgr.hiredate from employee e join employee mgr on e.mgr_id=mgr.id where e.hiredate<mgr.hiredate
   ~~~

7. 列出部门名称和这些部门的员工信息，同时列出那些没有员工的部门

   ~~~mysql
   select d.*,e.* from dept d left join employee e on d.id=e.dept_id
   ~~~

8. 列出所有‘clerk’（办事员）岗位的姓名以及部门名称

   ~~~mysql
   select e.name,d.name from employee e join dept d on e.dept_id=d.id where job='clerk'
   ~~~

9. 列出最低薪金大于6500的各种工作

   ~~~mysql
   先查询每种工作的最低薪金，对统计得到的结果再次筛选，筛选最低薪金大于6500的工作
   select job from employee group by job having min(salary)>6500
   ~~~

10. 列出在研发部工作的员工的姓名，假定不知道研发部的部门编号

    ~~~mysql
    select e.name from dept d join employee e on d.id=e.dept_id where d.name='研发部'
    ~~~
