SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `course_id` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `course_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `couse_desc` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`course_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1001', 'java基础', 'java基础学习');
INSERT INTO `course` VALUES ('1002', 'c++', 'c++学习');
INSERT INTO `course` VALUES ('1003', '数据结构与算法', '数据结果与算法学习');
INSERT INTO `course` VALUES ('1005', '单片机原理', '硬件学习');
INSERT INTO `course` VALUES ('1006', 'java高级', '软件学习');
INSERT INTO `course` VALUES ('1007', 'java中级', '软件中级学习');

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, '研发部');
INSERT INTO `dept` VALUES (2, '销售部');
INSERT INTO `dept` VALUES (3, '总裁办');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `salary` int(11) NULL DEFAULT NULL,
  `dept_id` int(11) NULL DEFAULT NULL,
  `job` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hiredate` date NULL DEFAULT NULL,
  `mgr_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'joe', 8500, 1, 'manager', '2021-07-03', 8);
INSERT INTO `employee` VALUES (2, 'henry', 8000, 2, 'manager', '2021-06-29', 8);
INSERT INTO `employee` VALUES (3, 'sam', 6000, 2, 'manager', '2021-06-29', 2);
INSERT INTO `employee` VALUES (4, 'max', 9000, 1, 'manager', '2021-07-02', 1);
INSERT INTO `employee` VALUES (5, 'janet', 6900, 1, 'developer', '2021-07-01', 1);
INSERT INTO `employee` VALUES (6, 'randy', 8500, 2, 'clerk', '2021-06-28', 3);
INSERT INTO `employee` VALUES (7, 'will', 7000, 1, 'developer', '2021-07-04', 1);
INSERT INTO `employee` VALUES (8, '马xx', 10000, 3, 'manager', '2021-06-01', NULL);

-- ----------------------------
-- Table structure for student_score
-- ----------------------------
DROP TABLE IF EXISTS `student_score`;
CREATE TABLE `student_score`  (
  `student_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `student_gender` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `course_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `score` double NULL DEFAULT NULL,
  `is_makeup` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student_score
-- ----------------------------
INSERT INTO `student_score` VALUES ('020654', '王强', '男', '1001', 55, '0');
INSERT INTO `student_score` VALUES ('020654', '王强', '男', '1002', 54, '0');
INSERT INTO `student_score` VALUES ('980412', '张丽', '女', '1001', 87, '0');
INSERT INTO `student_score` VALUES ('980423', '张鹏', '男', '1003', 78, '0');
INSERT INTO `student_score` VALUES ('990423', '李琼', '女', '1002', 56, '0');
INSERT INTO `student_score` VALUES ('990423', '李琼', '女', '1003', 45, '0');
INSERT INTO `student_score` VALUES ('020654', '王强', '男', '1001', 67, '1');
INSERT INTO `student_score` VALUES ('020654', '王强', '男', '1002', 65, '1');
INSERT INTO `student_score` VALUES ('990423', '李琼', '女', '1002', 66, '1');
INSERT INTO `student_score` VALUES ('990423', '李琼', '女', '1003', 77, '1');

SET FOREIGN_KEY_CHECKS = 1;