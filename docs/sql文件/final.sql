-- 导出 final 的数据库结构
CREATE DATABASE IF NOT EXISTS `final` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `final`;

-- 导出  表 final.course 结构
CREATE TABLE IF NOT EXISTS `course` (
  `course_id` varchar(5) NOT NULL,
  `course_name` varchar(30) DEFAULT NULL,
  `couse_desc` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  final.course 的数据：~6 rows (大约)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`course_id`, `course_name`, `couse_desc`) VALUES
	('1001', 'java基础', 'java基础学习'),
	('1002', 'c++', 'c++学习'),
	('1003', '数据结构与算法', '数据结果与算法学习'),
	('1005', '单片机原理', '硬件学习'),
	('1006', 'java高级', '软件学习'),
	('1007', 'java中级', '软件中级学习');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- 导出  表 final.course1 结构
CREATE TABLE IF NOT EXISTS `course1` (
  `cno` int(11) DEFAULT NULL,
  `cname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final.course1 的数据：~3 rows (大约)
DELETE FROM `course1`;
/*!40000 ALTER TABLE `course1` DISABLE KEYS */;
INSERT INTO `course1` (`cno`, `cname`) VALUES
	(1, '语文'),
	(2, '数学'),
	(3, '英语');
/*!40000 ALTER TABLE `course1` ENABLE KEYS */;

-- 导出  表 final.score 结构
CREATE TABLE IF NOT EXISTS `score` (
  `sno` int(11) DEFAULT NULL,
  `cno` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final.score 的数据：~0 rows (大约)
DELETE FROM `score`;
/*!40000 ALTER TABLE `score` DISABLE KEYS */;
INSERT INTO `score` (`sno`, `cno`, `score`) VALUES
	(1, 1, 60),
	(1, 2, 61),
	(2, 1, 80);
/*!40000 ALTER TABLE `score` ENABLE KEYS */;

-- 导出  表 final.student 结构
CREATE TABLE IF NOT EXISTS `student` (
  `sno` int(11) NOT NULL,
  `sname` varchar(50) DEFAULT NULL,
  `sage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final.student 的数据：~5 rows (大约)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`sno`, `sname`, `sage`) VALUES
	(1, '周杰伦', 18),
	(2, '周润发', 18),
	(3, '吴孟达', 25),
	(4, '刘德华', 25),
	(5, '李连杰', 29);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- 导出  表 final.student_score 结构
CREATE TABLE IF NOT EXISTS `student_score` (
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(50) DEFAULT NULL,
  `student_gender` varchar(2) DEFAULT NULL,
  `course_id` varchar(255) NOT NULL,
  `score` double DEFAULT NULL,
  `is_makeup` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  final.student_score 的数据：~10 rows (大约)
DELETE FROM `student_score`;
/*!40000 ALTER TABLE `student_score` DISABLE KEYS */;
INSERT INTO `student_score` (`student_id`, `student_name`, `student_gender`, `course_id`, `score`, `is_makeup`) VALUES
	('020654', '王强', '男', '1001', 55, '0'),
	('020654', '王强', '男', '1002', 54, '0'),
	('980412', '张丽', '女', '1001', 87, '0'),
	('980423', '张鹏', '男', '1003', 78, '0'),
	('990423', '李琼', '女', '1002', 56, '0'),
	('990423', '李琼', '女', '1003', 45, '0'),
	('020654', '王强', '男', '1001', 67, '1'),
	('020654', '王强', '男', '1002', 65, '1'),
	('990423', '李琼', '女', '1002', 66, '1'),
	('990423', '李琼', '女', '1003', 77, '1');
/*!40000 ALTER TABLE `student_score` ENABLE KEYS */;

-- 导出  表 final.stu_score 结构
CREATE TABLE IF NOT EXISTS `stu_score` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `bj` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final.stu_score 的数据：~6 rows (大约)
DELETE FROM `stu_score`;
/*!40000 ALTER TABLE `stu_score` DISABLE KEYS */;
INSERT INTO `stu_score` (`id`, `name`, `score`, `bj`) VALUES
	(1, '赵一', 89, '1班'),
	(2, '钱二', 88, '2班'),
	(3, '孙三', 84, '1班'),
	(4, '李四', 86, '2班'),
	(5, '王五', 87, '1班'),
	(6, '吴六', 91, '2班');
/*!40000 ALTER TABLE `stu_score` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
