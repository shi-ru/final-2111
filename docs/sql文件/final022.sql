-- 导出 final022 的数据库结构
CREATE DATABASE IF NOT EXISTS `final022` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `final022`;

-- 导出  表 final022.course 结构
CREATE TABLE IF NOT EXISTS `course` (
  `cno` int(11) NOT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `tno` int(11) DEFAULT NULL,
  PRIMARY KEY (`cno`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- 正在导出表  final022.course 的数据：~6 rows (大约)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`cno`, `cname`, `tno`) VALUES
	(1, 'Java基础', 1),
	(2, 'Java高级', 1),
	(3, 'C#', 2),
	(4, '计算机网络', 2),
	(5, '算法与结构', 4),
	(6, '数字模拟电子', 3);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- 导出  表 final022.sc 结构
CREATE TABLE IF NOT EXISTS `sc` (
  `sno` int(11) NOT NULL,
  `cno` int(11) DEFAULT NULL,
  `score` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- 正在导出表  final022.sc 的数据：~10 rows (大约)
DELETE FROM `sc`;
/*!40000 ALTER TABLE `sc` DISABLE KEYS */;
INSERT INTO `sc` (`sno`, `cno`, `score`) VALUES
	(1001, 1, 90),
	(1001, 2, 80),
	(1001, 3, 60),
	(1002, 1, 56),
	(1002, 2, 90),
	(1002, 4, 66),
	(1003, 1, 90),
	(1003, 2, 70),
	(1003, 4, 60),
	(1003, 3, 80);
/*!40000 ALTER TABLE `sc` ENABLE KEYS */;

-- 导出  表 final022.student 结构
CREATE TABLE IF NOT EXISTS `student` (
  `sno` int(11) NOT NULL,
  `sname` varchar(255) DEFAULT NULL,
  `sage` int(255) DEFAULT NULL,
  `ssex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sno`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- 正在导出表  final022.student 的数据：~3 rows (大约)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`sno`, `sname`, `sage`, `ssex`) VALUES
	(1001, 'tom', 22, '男'),
	(1002, 'jack', 23, '男'),
	(1003, 'rose', 24, '女');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- 导出  表 final022.teacher 结构
CREATE TABLE IF NOT EXISTS `teacher` (
  `tno` int(11) NOT NULL,
  `tname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tno`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- 正在导出表  final022.teacher 的数据：~4 rows (大约)
DELETE FROM `teacher`;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` (`tno`, `tname`) VALUES
	(1, '张力'),
	(2, '李四'),
	(3, '王强'),
	(4, '李晓燕');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;