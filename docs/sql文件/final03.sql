```
-- 导出  表 final031.student2 结构
CREATE TABLE IF NOT EXISTS `student2` (
  `id` int(11) DEFAULT NULL,
  `stu_no` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `course_id` varchar(50) DEFAULT NULL,
  `course_name` varchar(50) DEFAULT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final031.student2 的数据：~0 rows (大约)
DELETE FROM `student2`;
/*!40000 ALTER TABLE `student2` DISABLE KEYS */;
INSERT INTO `student2` (`id`, `stu_no`, `name`, `course_id`, `course_name`, `score`) VALUES
	(1, '2021001', '张三', '0001', '数学', 69),
	(2, '2021002', '李四', '0001', '数学', 89),
	(3, '2021001', '张三', '0001', '数学', 69);
/*!40000 ALTER TABLE `student2` ENABLE KEYS */;
```