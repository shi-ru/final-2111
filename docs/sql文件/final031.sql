SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `cno` int(11) NOT NULL,
  `cname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tno` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, 'Java基础', 1);
INSERT INTO `course` VALUES (2, 'Java高级', 1);
INSERT INTO `course` VALUES (3, 'C#', 2);
INSERT INTO `course` VALUES (4, '计算机网络', 2);
INSERT INTO `course` VALUES (5, '算法与结构', 4);
INSERT INTO `course` VALUES (6, '数字模拟电子', 3);

-- ----------------------------
-- Table structure for sc
-- ----------------------------
DROP TABLE IF EXISTS `sc`;
CREATE TABLE `sc`  (
  `sno` int(11) NOT NULL,
  `cno` int(11) NULL DEFAULT NULL,
  `score` int(255) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sc
-- ----------------------------
INSERT INTO `sc` VALUES (1001, 1, 90);
INSERT INTO `sc` VALUES (1001, 2, 80);
INSERT INTO `sc` VALUES (1001, 3, 60);
INSERT INTO `sc` VALUES (1002, 1, 56);
INSERT INTO `sc` VALUES (1002, 2, 90);
INSERT INTO `sc` VALUES (1002, 4, 66);
INSERT INTO `sc` VALUES (1003, 1, 90);
INSERT INTO `sc` VALUES (1003, 2, 70);
INSERT INTO `sc` VALUES (1003, 4, 60);
INSERT INTO `sc` VALUES (1003, 3, 80);

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `course` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `score` int(255) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('张三', '语文', 81);
INSERT INTO `score` VALUES ('张三', '数学', 80);
INSERT INTO `score` VALUES ('李四', '语文', 76);
INSERT INTO `score` VALUES ('王五', '语文', 81);
INSERT INTO `score` VALUES ('王五', '数学', 100);
INSERT INTO `score` VALUES ('王五', '英语', 90);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `sno` int(11) NOT NULL,
  `sname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sage` int(255) NULL DEFAULT NULL,
  `ssex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1001, 'tom', 22, '男');
INSERT INTO `student` VALUES (1002, 'jack', 23, '男');
INSERT INTO `student` VALUES (1003, 'rose', 24, '女');

-- ----------------------------
-- Table structure for student1
-- ----------------------------
DROP TABLE IF EXISTS `student1`;
CREATE TABLE `student1`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student1
-- ----------------------------
INSERT INTO `student1` VALUES (1, '张三');
INSERT INTO `student1` VALUES (2, '张三');
INSERT INTO `student1` VALUES (3, '李四');
INSERT INTO `student1` VALUES (4, '王五');
INSERT INTO `student1` VALUES (5, '王五');
INSERT INTO `student1` VALUES (6, '王五');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `tno` int(11) NOT NULL,
  `tname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, '张力');
INSERT INTO `teacher` VALUES (2, '李四');
INSERT INTO `teacher` VALUES (3, '王强');
INSERT INTO `teacher` VALUES (4, '李晓燕');

SET FOREIGN_KEY_CHECKS = 1;