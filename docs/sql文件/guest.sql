-- 导出  表 final04.guest 结构
CREATE TABLE IF NOT EXISTS `guest` (
  `accounts` varchar(50) DEFAULT NULL,
  `details` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final04.guest 的数据：~0 rows (大约)
DELETE FROM `guest`;
/*!40000 ALTER TABLE `guest` DISABLE KEYS */;
INSERT INTO `guest` (`accounts`, `details`, `date`, `money`, `class`) VALUES
	('s0001', '房费', '2022-03-15', 280, '001'),
	('s0001', '酒水', '2022-03-16', 120, '001'),
	('s0001', '房费', '2022-03-08', 300, '003'),
	('s0002', '酒水', '2022-03-29', 50, NULL),
	('s0003', '房费', '2022-03-31', 180, '002'),
	('s0004', '房费', '2022-04-01', 230, '001'),
	('s0005', '酒水', '2022-04-01', 100, NULL),
	('s0005', '房费', '2022-04-02', 128, '001');
/*!40000 ALTER TABLE `guest` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;