-- 导出  表 final031.match_record 结构
CREATE TABLE IF NOT EXISTS `match_record` (
  `match_date` date DEFAULT NULL,
  `result` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  final031.match_record 的数据：~0 rows (大约)
DELETE FROM `match_record`;
/*!40000 ALTER TABLE `match_record` DISABLE KEYS */;
INSERT INTO `match_record` (`match_date`, `result`) VALUES
	('2022-03-14', '胜'),
	('2022-03-14', '胜'),
	('2022-03-14', '负'),
	('2022-03-14', '负'),
	('2022-03-14', '负'),
	('2022-03-13', '胜'),
	('2022-03-13', '负'),
	('2022-03-13', '负');
/*!40000 ALTER TABLE `match_record` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;