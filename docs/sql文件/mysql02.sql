DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES (1, '一班');
INSERT INTO `class` VALUES (2, '二班');
INSERT INTO `class` VALUES (3, '三班');
INSERT INTO `class` VALUES (4, '四班');
INSERT INTO `class` VALUES (5, '五班');

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `stu_id` int(11) NOT NULL,
  `chinese` int(255) NULL DEFAULT NULL,
  `math` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`stu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES (1, 70, 47);
INSERT INTO `score` VALUES (2, 80, 60);
INSERT INTO `score` VALUES (3, 50, 82);
INSERT INTO `score` VALUES (4, 80, 90);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stu_no` int(11) NULL DEFAULT NULL,
  `stu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `stu_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `class_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 2020001, '张三', '男', 1);
INSERT INTO `student` VALUES (2, 2020002, '李四', '男', 1);
INSERT INTO `student` VALUES (3, 2020003, '李丽', '女', 2);
INSERT INTO `student` VALUES (4, 2020004, '赵婷', '女', 3);
INSERT INTO `student` VALUES (5, 2020005, '王五', '男', 3);

SET FOREIGN_KEY_CHECKS = 1;