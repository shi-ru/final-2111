package com.example.final2111;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Final2111Application {

    public static void main(String[] args) {
        SpringApplication.run(Final2111Application.class, args);
    }

}
