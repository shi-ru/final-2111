package com.example.final2111.day14;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] ary = {21,2,11,0,6,89,4};
        //使用冒泡排序对数组进行升序排列
        //外层for循环表示轮数
//        for (int i=0;i<ary.length-1;i++){
//            //内存for循环表示从下表0开始遍历元素，随着轮数的增加，遍历的最大下标在减小
//            for (int j=0;j<ary.length-1-i;j++){
//                if (ary[j]>ary[j+1]){
//                    int tmp = ary[j];
//                    ary[j]=ary[j+1];
//                    ary[j+1] = tmp;
//                }
//            }
//        }
//        System.out.println(Arrays.toString(ary));
        for (int i=0;i<ary.length-1;i++){
            //内存for循环表示从下表0开始遍历元素，随着轮数的增加，遍历的最大下标在减小
            for (int j=ary.length-1;j>0;j--){
                if (ary[j]>ary[j-1]){
                    int tmp = ary[j];
                    ary[j]=ary[j-1];
                    ary[j-1] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(ary));

    }
}
