package com.example.final2111.day14;

import java.util.Arrays;

public class BubbleSortDeno02 {
    public static void main(String[] args) {
        String[] ary = {"tom","amy","jack","rose","张三","李四","中国"};
        char c='中';
        int c1 = c;
        char d='张';
        int d1 = d;
        //使用冒泡排序对String[]进行升序排列
        for (int i=0;i<ary.length-1;i++){
            for (int j=0;j<ary.length-1-i;j++){
                if (ary[j].compareTo(ary[j+1])>0){
                    String tmp = ary[j];
                    ary[j]= ary[j+1];
                    ary[j+1]=tmp;
                }
            }
        }
        System.out.println(Arrays.toString(ary));
        System.out.println(c1);
        System.out.println(d1);
    }

}
