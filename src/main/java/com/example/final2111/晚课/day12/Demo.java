package com.example.final2111.晚课.day12;

public class Demo {
    public static void main(String[] args) {
        String str = "AAAAAABBBCCCCCDDDBDDDD";
        //1. 将字符串转换为字符数组
        char[] chs = str.toCharArray();
        //2. 遍历字符数组，判断遍历道的字符与后一位是否相等
        int count = 1;
        StringBuilder builder = new StringBuilder();
        for (int i=0;i<chs.length-1;i++){
            //判断字符与后一位是否相等
            if (chs[i]==chs[i+1]){
                count++;

            }else {  //字符连续出现中断
                builder.append(chs[i]).append(count);
                count=1;
            }
        }
        //将最后一次出现的字符及其次数手动存入builder中
        builder.append(chs[chs.length-1]).append(count);
        String dest = builder.toString();
        System.out.println(dest);
    }
}
