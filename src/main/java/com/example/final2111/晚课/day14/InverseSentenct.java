package com.example.final2111.晚课.day14;

public class InverseSentenct {
    public static void main(String[] args) {
        String in="I.love.the.game";
        String sep=".";
        String newStr = inverse(in,sep);
        System.out.println(newStr);
    }

    static String inverse(String in,String sep){
        /*
        fromIndex: 从哪个位置开始逆序查找
        index:指向查找到的分隔符的下标
        start:从哪里开始截取字符串
        end:截取字符串的结束下标
         */
        int fromIndex = in.length()-1,index=-1,start,end=in.length();
        StringBuilder builder = new StringBuilder();

        while ((index=in.lastIndexOf(sep,fromIndex))!=-1){
            //说明分隔符出现，则开始截取
            start = index+sep.length();
            String part = in.substring(start,end);
            builder.append(part).append(sep);
            //一次截取结束，移动end和fromIndex
            end = index;
            fromIndex = index-1;
        }

        //手动截取第一部分并拼接入StringBuilder中
        String first = in.substring(0,end);
        return builder.append(first).toString();
    }
}
