package com.example.final2111.晚课.day15;

import java.util.Scanner;

public class ThreeSplit{
    public static void main(String[] args) {
        //让用户输入目标数字字符串
        System.out.println("请输入数字字符串：");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String newStr = threeSplit(line);
        System.out.println(newStr);
    }

    public static String threeSplit(String input){
        //需要对目标字符串倒序遍历，将遍历到的元素存入StringBuilder中，且进行计数，根据计数器的结果是否是3的倍数来决定是否拼接，
        StringBuilder builder = new StringBuilder();
        int count = 0;
        for (int i=input.length()-1;i>=0;i--){
            char c = input.charAt(i);
            builder.append(c);
            count++;
            //判断计数器是否是3的倍数
            if (count%3==0 && i!=0)
                builder.append(',');
        }
        return builder.reverse().toString();

//        // TODO Auto-generated method stub
//        Scanner sc = new Scanner(System.in);
//        System.out.println("请输入一个字符串:");
//        String nums = sc.next();
//        //接受字符，放在StringBuffer里；
//        //StringBuilder str = new StringBuilder(nums);
//        StringBuffer str = new StringBuffer(nums);
//        //从后往前每隔三位插入一个逗号；
//        for (int i = str.length() - 3; i > 0; i = i - 3) {
//            str.insert(i, ",");
//        }
//        System.out.print("结果为： "+str);
    }
}
