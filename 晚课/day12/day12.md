完成到7：45

1. 写一段小程序统计字符串中的连续字母的个数，使“AAAAAABBBCCCCCDDDBDDDD”变为“A6B3C5D3B1D4”

   ~~~java
   核心：统计每个字符连续出现的次数
   思路：
       1. 先将字符串转换为字符数组   toCharArray(): char[]
       2. 对字符数组进行遍历，遍历到倒数第二位，每遍历一个字符，判断其与后一位是否相等，（如何判断字符是否相等：用==，因为char保存的时候是以整数的形式保存的）
       	若相等，说明字符连续出现，让计数器+1（count，初始值为1）
       	若不相等，说明字符连续出现中断，此时让字符与次数保存起来（通过StringBuilder）
           并让计数器进行归1，重新统计新的字符连续出现的次数
       3. 直到最后一次出现的字符，后面没有与其不相等的字符，所以最后一次出现的字符及其次数并不会存入StrignBuilder中，需要手动存储。
       
   public static void main(String[] args) {
           String str = "AAAAAABBBCCCCCDDDBDDDD";
           //1. 将字符串转换为字符数组
           char[] chs = str.toCharArray();
           //2. 遍历字符数组，判断遍历道的字符与后一位是否相等
           int count = 1;
           StringBuilder builder = new StringBuilder();
           for (int i=0;i<chs.length-1;i++){
               //判断字符与后一位是否相等
               if (chs[i]==chs[i+1]){
                   count++;
               }else {  //字符连续出现中断
                   builder.append(chs[i]).append(count);
                   count=1;
               }
           }
           //将最后一次出现的字符及其次数手动存入builder中
           builder.append(chs[chs.length-1]).append(count);
           String dest = builder.toString();
           System.out.println(dest);
       }    		
   ~~~

   





